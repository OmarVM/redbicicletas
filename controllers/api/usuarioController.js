var Usuario = require('../../models/modeloUsuario');
var Bicicleta = require('../../models/modeloBicicleta');
var Reserva = require('../../models/modeloReserva');

exports.crearUsuario = function(req, res){
    var usuario = new Usuario({nombre: req.body.nombre, email: req.body.email, password: req.body.password});
    usuario.save();
    console.log(usuario)
    res.status(200).json(usuario)
}


exports.obtenerUsuario = function(req,res){
    var id = req.body.id
    Usuario.findById(id, function(err,user){
        res.status(200).json({
            usuario: user
        })
    })
}

exports.reservarBicicleta = function(req, res){

    Usuario.findById({_id: req.body.id}, function(err, user){
        Bicicleta.findByCode(req.body.bici, function(err, bic){
            user.reservarBicicleta(bic.id, req.body.desde, req.body.hasta, function(err, reserva){
                Reserva.find({}).populate('usuario').populate('bicicleta').exec(function(err, reservas){
                    res.status(200).json({
                        reserva: reservas
                    })
                })
            })
        })
    })
    
}