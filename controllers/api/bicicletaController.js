var Bicicleta = require('../../models/modeloBicicleta');

exports.listaBicicletas = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({
            listaBicicletas: bicis
        })
    })
}

exports.crearBicicleta = function(req, res){
    var bicicleta = new Bicicleta({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
        })

        Bicicleta.add(bicicleta, function(err, nuevaBici){
            res.status(200).json(nuevaBici)
        })
}

exports.obtenerPorCodigo = function(req, res){
    Bicicleta.findByCode(req.body.code, function(err,bici){
        res.status(200).json({
            bicicleta: bici
        })
    })
}

exports.borrarBicicleta = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(err, callback){
        res.status(200).json(callback)
    })
}

exports.actualizarBicicleta = function(req, res){
    
    var currentCode = req.body.code;

    Bicicleta.findOneAndUpdate({code: currentCode}, 
        {code: req.body.n_code,
        modelo: req.body.modelo,
        color: req.body.color,
        ubicacion: req.body.ubicacion
    }, function(err, n_bici){
        if (!err){
            Bicicleta.findByCode(req.body.n_code, function(err,bici){
                res.status(200).json({
                    bicicleta: bici
                })
            })
        }
    })

}