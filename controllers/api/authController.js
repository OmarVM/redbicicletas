const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Usuario = require('../../models/modeloUsuario');

module.exports = {
    authenticate: function(req, res, next){
        Usuario.findOne({email: req.body.email}, function(err, userInfo){
            if (err){
                next(err)
            } else {
                if (userInfo === null) {
                    return res.status(401).json({status: 'error', message: 'Usuario no encontrado', data: null})
                }
                if (userInfo != null && bcrypt.compareSync(req.body.password, userInfo.password)){
                    userInfo.save(function(err, user){
                        const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'),{ expiresIn: '7d'});
                        res.status(200).json({message: 'Usuario encontrado', data: { usuario: userInfo, token: token}})
                    })
                } else {
                    res.status(401).json({status: 'error', message: 'E-mail/contraseña invalida', data: null})
                    }
                }
            })
        },

        forgotPassword: function(req, res, next){
            Usuario.findOne({email: req.body.email}, function(err, user){
                if (!user){
                    return res.status(401).json({status: 'error', message: 'E-mail/contraseña invalida', data: null});
                }
                user.resetPassword(function(err){
                    if (err){
                        return next(err);
                    }
                    res.status(200).json({message: 'Se envío mail de reestablecimiento de contraseña', data: null})
                })
            })
        },

        authFacebookToken: function(req, res, next){
            if (req.user){
                req.user.save().then( () => {
                    const token = jwt.sign({id: req.user.id}, req.app.get('secretKey'), {expiresIn: '7d'});
                    res.status(200).json({message: 'Usuario encontrado o creado', data: {user: req.user, token: token}})
                }).catch((err) => {
                    console.log(err);
                    res.status(500).json({message: err.message})
                })
            }
        }
    }