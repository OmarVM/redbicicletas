var Bicicleta = require('../models/modeloBicicleta')

exports.listaBicicletas = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).render('bicicletas/index', {bicicletas: bicis});
    })
}

exports.crearBicicletaGet = function(req, res){
    res.render('bicicletas/create');
}

/*
exports.actualizarBicicletaGet = function(req, res){
    var bici = Bicicleta.findByCode(req.params.code);

    console.log(bici)

    res.status(200).render('bicicletas/update', {bici: bici});
}*/

exports.actualizarBicicletaGet = function(req, res){
    Bicicleta.findByCode(req.params.code, function(err, bicicleta){
        res.status(200).render('bicicletas/update', {bici: bicicleta})
    })
}

exports.actualizarBicicletaPost = function(req, res){
    /*var bici = Bicicleta.findByCode(req.params.code);

    bici.code = req.params.code;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.long];

    res.redirect('/bicicletas');*/

    var updateValues = {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.long]
    }

    Bicicleta.updateByCode(req.params.code, updateValues, function(err, bici){
        res.status(200).redirect('/bicicletas');
    })
}

exports.crearBicicletaPost = function(req, res){
    var ubicacion = [req.body.lat,req.body.long]
    var bici = new Bicicleta({
        code: req.body.code, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: ubicacion
        })
    
    Bicicleta.add(bici, function(err, bici){
        res.status(200).redirect('/bicicletas');
    });
}

exports.eliminarBicicletaPost = function(req,res){
    Bicicleta.removeByCode(req.params.code, function(err,cb){   
        res.status(200).redirect('/bicicletas');
    });
}