var nodemailer = require('nodemailer')
const sgTransport = require('nodemailer-sendgrid-transport')

let mailConfig;

if (process.env.NODE_ENV === 'Production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_KEY
        }
    }
    mailConfig = sgTransport(options)
} else {
    if (process.env.NODE_ENV === 'Staging'){
        console.log('XXXXXXXXXXXXXX')
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_KEY
            }
        }
        mailConfig = sgTransport(options)
    } else {
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ETHEREAL_USER,
                pass: process.env.ETHEREAL_PASSWORD
            }
        };
        //mailConfig = sgTransport(transporter)
    }
}



module.exports = nodemailer.createTransport(mailConfig)