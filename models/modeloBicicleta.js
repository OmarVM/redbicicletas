var mongoose = require('mongoose');
const Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true }
        //Los indices (indexes) son usados para optimizar busquedas grandes
        //Acá estamos diciendo "Ubicacion es un objeto del typo Array of Numbers (por eso los corchetes[])
        //Que posee un índice del tipo 2dsphere (tipo de dato geográfico)"
    }
});

BicicletaSchema.methods.toString = function(){
    return 'Code: ' + this.code + '| . Color: '+ this.color;
}

//.statics = Se agrega el metodo al modelo directamente
BicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb)
}

BicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    })
}

BicicletaSchema.statics.add = function(bici, cb){
    this.create(bici,cb);
}

BicicletaSchema.statics.findByCode = function(codigo, cb){
    return this.findOne({code: codigo}, cb);
}

BicicletaSchema.statics.removeByCode = function(codigo, cb){
    return this.deleteOne({code: codigo}, cb);
}

BicicletaSchema.statics.updateByCode = function(codigo, values, cb){
    return this.findOneAndUpdate({code: codigo}, values, cb)
}

module.exports = mongoose.model('Bicicleta', BicicletaSchema);

/*
var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}
*/
/*
Bicicleta.prototype.toString = function(){
    return 'id: ' + this.id + '. Color: '+ this.color;
}*/

/*
Bicicleta.allBicis = []
*/
//Methods
/*
Bicicleta.add = function(bici){
    Bicicleta.allBicis.push(bici);
}

Bicicleta.findById = function(id){
    var bicicleta = Bicicleta.allBicis.find(x => x.id == id);
    
    if (bicicleta){
        return bicicleta;
    } else {
        throw new Error(`No existe una bicicleta con el ID: ${id}`);
    }
}

Bicicleta.removeById = function(id){
    for (var i = 0; i<Bicicleta.allBicis.length;i++){
        if (Bicicleta.allBicis[i].id == id){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}

var a = new Bicicleta(1, "Azul","BMX",[-34.6012424,-58.3861497]);
var b = new Bicicleta(2, "Negro","Montaña",[-34.596932,-58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;*/