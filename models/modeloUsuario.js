var mongoose = require('mongoose');
var Reserva = require('./modeloReserva');
var Token = require('./modeloToken');
var bcrypt = require('bcrypt');
var crypto = require('crypto');
var mailer = require('../mailer/mailer')
var uniqueValidator = require('mongoose-unique-validator')
var Schema = mongoose.Schema;


const saltRounds = 10; //Introduce aleatoreidad a la encriptación.

//Funciones de validación
const validarEmail = function(mail){
    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; //Objeto Regex
    return re.test(mail);
}

var UsuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El e-mail es obligatorio'],
        lowercase: true,
        validate: [validarEmail, 'Por favor ingrese un E-Mail valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

//Funciones de Esquema -> Queries
UsuarioSchema.statics.createInstance = function(nombre){
    return new this({
        nombre: nombre
    })
}

UsuarioSchema.methods.reservarBicicleta = function(biciCode, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciCode, desde: desde, hasta: hasta});
    reserva.save(cb);
}

UsuarioSchema.statics.findById = function(id, cb){
    return this.findOne({_id: id}, cb)
}

UsuarioSchema.pre('save', function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds)
    }
    next();
});

UsuarioSchema.methods.validatePassword = function(pass){
    return bcrypt.compareSync(pass, this.password)
}

UsuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const destinoEmail = this.email;

    token.save(function(err){
        if (err) { return cb(err) }

        const mailOptions = {
            from: 'no-reply@redBicicletas.com',
            to: destinoEmail,
            subject: 'Reestablecimiento de Contraseña',
            text: 'Bienvenido a nuestra Red de Bicicletas \n\n Para reestablecer su contraseña, por favor haz click en este link: \n http://localhost:3000' + '\/resetPassword\/' + token.token + '. \n'
        }

        mailer.sendMail(mailOptions, function(err){
            if (err) { return cb(err)}
        })

        console.log('A password reset mail has been sent to '+ destinoEmail + '.')
    })

    cb(null);
}

UsuarioSchema.statics.findOneOrCreateByGoogle = function(condition, cb){
    const self = this;
    self.findOne({
        $or: [
            {'googleId': condition.id},
            {'email': condition.emails[0].value}
        ]
    }, (err, result) => {
        if (result){
            callback(err, result);
        } else {
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName;
            values.verificado = true;
            values.password = condition._json.etag;

            self.create(values, (err, result) =>{
                if (err) { console.log(err) }
                return callback(err,result)
            })
        }
    })
}

UsuarioSchema.statics.findOneOrCreateByFacebook = function(condition, cb){
    const self = this;
    self.findOne({
        $or: [
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}
        ]
    }, (err, result) => {
        if (result){
            callback(err, result);
        } else {
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName;
            values.verificado = true;
            values.password = condition._json.etag;

            self.create(values, (err, result) =>{
                if (err) { console.log(err) }
                return callback(err,result)
            })
        }
    })
}

//Plugins
UsuarioSchema.plugin(uniqueValidator, `El {PATH} ya existe como otro usuario`)

UsuarioSchema.methods.enviarMailBienvenida = function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const destinoEmail = this.email;

    token.save(function(err){
        if (err) { return console.log(err.message) }

        const mailOptions = {
            from: 'omarrgvm.97@gmail.com',
            to: destinoEmail,
            subject: 'Verificacion de Cuenta',
            text: 'Bienvenido a nuestra Red de Bicicletas \n\n Para verificar tu cuenta, por favor haz click en este link: \n http://localhost:3000' + '\/token/confirmation\/' + token.token + '. \n'
        }

        mailer.sendMail(mailOptions, function(err){
            if (err) { return console.log(err.message)}
        })

        console.log('A verification mail has been sent to '+ destinoEmail + '.')
    })
}

module.exports = mongoose.model('Usuario', UsuarioSchema);