var Bicicleta = require('../../models/modeloBicicleta');
var Usuario = require('../../models/modeloUsuario');
var Reserva = require('../../models/modeloReserva');
var request = require('request')
var mongoose = require('mongoose');

const baseUrl = 'http://localhost:3000/api/usuarios';

describe('Testing de modelo de usuario', function(){
    beforeEach(function(done){
        var mongodb = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongodb, {useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongo DB connection error'))
        db.once('open', function(){
            console.log('Conectamos exitosamente a la DB');
            done();
        });
    });

    afterEach(function(done){
        const db = mongoose.connection
        Reserva.deleteMany({}, function(err,success){
            if (err){
                console.log(err);
            }
            Usuario.deleteMany({}, function(err, success){
                if (err){
                    console.log(err);
                }
                Bicicleta.deleteMany({},function(err, success){
                    if (err){
                        console.log(err);
                    }
                    db.close()
                    done()
                })

            })

        })
    })

    describe('Prueba creacion de usuario', () => {
        it('Comprueba que los usuarios se creen correctamente', function(done){
            
            request.post({
                headers: {'content-type':'application/json'},
                url: baseUrl + '/create',
                body: '{"nombre": "Omar Vivenes"}'
            }, (error, response, body) => {
                var nuevoUsuario = JSON.parse(body);
                expect(nuevoUsuario.nombre).toBe("Omar Vivenes")

                done()
            })
        })
    })
    
    describe('Prueba de reserva de bicicleta', () => {
        it('Comprueba que se puedan hacer reservas sin problemas', (done) => {
            const usuario = new Usuario({nombre: "Omar Vivenes"});
            usuario.save();

            const bici = new Bicicleta({code: 2,color: "Negra", modelo: "BMX", ubicacion: [-34.5,-48.3]});
            bici.save();

            var hoy = new Date()
            var mañana = new Date().setDate(hoy.getDate() + 1)

            usuario.reservarBicicleta(bici.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(2);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                })
            })
        })
    })
    /*
    describe('Prueba obtenerUsuario', () => {
        it('Obtiene un usuario existente', (done) => {
            request.post({
                headers: {'content-type':'application/json'},
                url: baseUrl + '/create',
                body: '{"nombre": "Omar Vivenes"}'
            }, (error, response, body) => {
               request.get({
                    headers: {'content-type':'application/json'},
                    url: baseUrl + '/obtener',
                    body: '{"usuario": "Omar Vivenes"}'
               }, (error, response, body) => {
                   expect(body).toBe('true')
                   done()
               }) 
            })
        })
    })*/
})