var Bicicleta = require('../../models/modeloBicicleta');
var mongoose = require('mongoose');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongodb = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongodb, {useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongo DB connection error'))
        db.once('open', function(){
            console.log('Conectamos exitosamente a la DB');
            done();
        });
    });

    afterEach(function(done){
        const db = mongoose.connection
        Bicicleta.deleteMany({}, function(err,success){
            if (err){
                console.log(err);
            }
            db.close()
            done()
        })
    })

    describe('Bicicleta.createInstance', () =>{
        it('Crea una instancia de bicicleta', () =>{
            var bici = Bicicleta.createInstance(1,"Verde","Urbana",[-34.5,-48.3]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-48.3);
        });
    });

    describe('Bicicletas.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done()
            })
        })
    })

    
    describe('Bicicletas.add', () => {
        it('Agrega una bici', (done) => {
            var bici = new Bicicleta({code: 2,color: "Negra", modelo: "BMX", ubicacion: [-34.5,-48.3]})
            Bicicleta.add(bici, function(err, nuevaBici){
                if (err) { console.log(err) };
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(2);
    
                    done()
                });
            });
        });
    });

    describe('Bicicletas.findByCode', () => {
        it('Debe verificar que la bici especificada este bien', (done) => {


            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var bici1 = new Bicicleta({code: 2,color: "Negra", modelo: "BMX", ubicacion: [-34.5,-48.3]})
                Bicicleta.add(bici1, function(err, nuevaBici){
                    if (err) { console.log(err) };

                    var bici2 = new Bicicleta({code: 3,color: "Rojo", modelo: "BMX", ubicacion: [-34.5,-48.3]})
                    Bicicleta.add(bici2, function(err, nuevaBici){
                        if (err) { console.log(err) };

                        Bicicleta.findByCode(2, function(error, targetBici){
                            expect(targetBici.code).toBe(2);
                            expect(targetBici.color).toBe("Negra");
                            expect(targetBici.modelo).toBe("BMX");

                            done();
                        });
                    })
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Debe remover un item por codigo', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var bici1 = new Bicicleta({code: 2,color: "Negra", modelo: "BMX", ubicacion: [-34.5,-48.3]})
                Bicicleta.add(bici1, function(err, nuevaBici){
                    if (err) { console.log(err) };

                    var bici2 = new Bicicleta({code: 3,color: "Rojo", modelo: "BMX", ubicacion: [-34.5,-48.3]})
                    Bicicleta.add(bici2, function(err, nuevaBici){
                        if (err) { console.log(err) };

                        Bicicleta.removeByCode(2, function(error, targetBici){
                            Bicicleta.allBicis(function(err, bicis){
                                expect(bicis.length).toBe(1);
                                expect(bicis[0].code).toBe(3);
                                done();
                            });
                        });
                    })
                });
            });
        })
    })
})
/*
var Bicicleta = require('../../models/modeloBicicleta')

beforeEach(function(){
    console.log('testeando...')
    Bicicleta.allBicis = []
})

describe('Bicicleta.allBicis',function(){
    it('Comienza vacía', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})


describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        var bici = new Bicicleta(3, "Verde", "BMX", [-34.6012424,-58.3861497])
        Bicicleta.add(bici)

        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(bici)
    })
})

describe('Bicicleta.findById', function() {
    it('Debe devolver la bici de ID 3', function(){
        expect(Bicicleta.allBicis.length).toBe(0)

        var bici = new Bicicleta(3, "Verde", "BMX", [-34.6012424,-58.3861497])
        Bicicleta.add(bici)

        var targetBici = Bicicleta.findById(3)
        expect(targetBici.id).toBe(3)
        expect(targetBici.color).toBe(bici.color)
        expect(targetBici.modelo).toBe(bici.modelo)
    })
})

describe('Bicicleta.removeById', () => {
    it('Debe remover la Bici de ID 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0)

        var bici = new Bicicleta(1, "Verde", "BMX", [-34.6012424,-58.3861497])
        Bicicleta.add(bici)

        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.findById(1).id).toBe(1)
        
        Bicicleta.removeById(1)
        
        expect(Bicicleta.allBicis.length).toBe(0)

    })
})

*/