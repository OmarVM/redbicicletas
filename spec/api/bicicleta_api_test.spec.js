var Bicicleta = require('../../models/modeloBicicleta')
var request = require('request')
var mongoose = require('mongoose')
var server = require('../../bin/www');
const { base } = require('../../models/modeloBicicleta');

const baseUrl = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {

    beforeEach(function(done){
        var mongodb = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongodb, {useNewUrlParser: true, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Mongo DB connection error'))
        db.once('open', function(){
            console.log('Conectamos exitosamente a la DB');
            done();
        });
    });

    afterEach(function(done){
        const db = mongoose.connection
        Bicicleta.deleteMany({}, function(err,success){
            if (err){
                console.log(err);
            }
            db.close()
            done()
        })
    })

    describe('Get Bicicletas', () => {
        it('Obtiene el listado de bicis por API Call', (done) => {

            request.get(baseUrl, function(error,response,body){
                expect(response.statusCode).toBe(200);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(0);
                    done()
                });
            })
        })
    })

    
    describe('Post Bicicletas', function() {
        it('Da de alta una nueva bicicleta por API call', function(done){
            var headers = {'content-type':'application/json'}
            var bici = '{"code":10,"color":"Negro","modelo":"BMX","ubicacion":[-34.5,-48.6]}'
            request.post({
                headers: headers,
                url: baseUrl + '/create',
                body: bici
            }, (error,response,body) => {
                expect(response.statusCode).toBe(200);
                expect(JSON.parse(body).code).toBe(10)
                done()
            })
        })
    });

    describe('Post Remover Bicicletas', function() {
        it('Borra una bicicleta por API call', function(done){
            var headers = {'content-type':'application/json'}
            var bici1 = '{"code":10,"color":"Negro","modelo":"BMX","ubicacion":[-34.5,-48.6]}'
            var bici2 = '{"code":9,"color":"Azul","modelo":"BMX","ubicacion":[-34.5,-48.6]}'

            request.post({
                headers: headers,
                url: baseUrl + '/create',
                body: bici1
            }, (error, response, body) => {
                request.post({
                    headers: headers,
                    url: baseUrl + '/create',
                    body: bici2
                }, (error, response, body) => {
                    request.delete({
                        headers: headers,
                        url: baseUrl + '/delete',
                        body: '{"code":10}'
                    }, (error, response, body) =>{
                        expect(response.statusCode).toBe(200)
                        Bicicleta.allBicis(function(err, bicis){
                            expect(bicis.length).toBe(1);
                            expect(bicis[0].code).toBe(9)
                            done()
                        });
                    })
                })

            })
        })
    })

    describe('Update de Bicicletas', function(){
        it('Actualiza un documento existente en la DB', (done) => {
            var headers = {'content-type':'application/json'}
            var bici = '{"code":10,"color":"Negro","modelo":"BMX","ubicacion":[-34.5,-48.6]}'
            var updatedBici = '{"code":10, "n_code": 9 ,"color":"Azul","modelo":"BMX","ubicacion":[-34.5,-48.6]}'

            request.post({
                headers: headers,
                url: baseUrl + '/create',
                body: bici
            }, (error, response, body) =>{
                request.put({
                    headers: headers,
                    url: baseUrl + '/update',
                    body: updatedBici
                }, (error, response, body) =>{
                    expect(response.statusCode).toBe(200)
                    Bicicleta.allBicis(function(err, bicis){
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toBe(9);
                        expect(bicis[0].color).toBe("Azul");
                        done()
                    });
                })
            })
        })
    })
})