var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy
var Usuario = require('../models/modeloUsuario');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new LocalStrategy(
    function(email, password, done) {
        Usuario.findOne({email: email}, function(err, user){
            if (err){
                return done(err);
            }
            if (!user){
                return done(null, false, {message: 'E-mail incorrecto o inexistente'});
            }
            if (!user.validatePassword(password)){
                return done(null, false, {message: 'Contraseña incorrecta'});
            }
            //Se debería validar si el usuario está confirmado
            return done(null, user);
        })
    }
))

passport.use(new GoogleStrategy({
    clientID:     process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + '/auth/google/callback',
    passReqToCallback   : true
  },
  function(request, accessToken, refreshToken, profile, done) {
    Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
      return done(err, user);
    });
  }
));

passport.use(new FacebookTokenStrategy({
    clientID:     process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
  },
  function(request, accessToken, refreshToken, profile, done) {
      try{
        Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
            if (err) {console.log(err)}
            return done(err, user);
          });
      } catch (err2) {
        console.log(err2);
        return done(err2,null)
      }
  }
));

passport.serializeUser(function(user, cb){
    cb(null,user.id)
})

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario)
    })
})

module.exports = passport