var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
  //Para renderizar una vista usamos el metodo .render() que toma como params:
  //-> Nombre del template, en este casi "index"
  //-> Atributo y su respectivo valor, en este caso { title: 'Express' }
});

module.exports = router;
