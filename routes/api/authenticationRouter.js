var express = require('express')
var router = express.Router();
var auth = require('../../controllers/api/authController');
var passport = require('../../config/passport');

router.post('/authenticate', auth.authenticate);
router.post('/forgotPassword', auth.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), auth.authFacebookToken);

module.exports = router;