var express = require('express')
var usuario = require('../../controllers/api/usuarioController')
var router = express.Router();

router.get('/', usuario.obtenerUsuario);
router.post('/create',usuario.crearUsuario);
router.post('/reserve',usuario.reservarBicicleta);

module.exports = router;