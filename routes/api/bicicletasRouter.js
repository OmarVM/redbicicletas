var express = require('express')
var router = express.Router();
var bicicleta = require('../../controllers/api/bicicletaController')

router.get('/', bicicleta.listaBicicletas);
router.get('/getByCode', bicicleta.obtenerPorCodigo);
router.post('/create',bicicleta.crearBicicleta);
router.put('/update', bicicleta.actualizarBicicleta)
router.delete('/delete',bicicleta.borrarBicicleta);

module.exports = router;