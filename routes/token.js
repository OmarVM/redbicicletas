var express = require('express');
var router = express.Router();
var token = require('../controllers/controladorToken');

router.get('/confirmation/:token', token.confirmationGet);

module.exports = router;