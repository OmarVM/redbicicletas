var express = require('express')
var router = express.Router();
var bicicleta = require('../controllers/controladorBicicleta')

router.get('/',bicicleta.listaBicicletas);
router.get('/create', bicicleta.crearBicicletaGet);
router.get('/:code/update', bicicleta.actualizarBicicletaGet)
router.post('/create', bicicleta.crearBicicletaPost);
router.post('/:code/delete',bicicleta.eliminarBicicletaPost);
router.post('/:code/update', bicicleta.actualizarBicicletaPost);

module.exports = router;