var express = require('express');
var router = express.Router();
var usuario = require('../controllers/controladorUsuario')

/* GET users listing. */
router.get('/', usuario.list);
router.get('/signup', usuario.createGet);
router.post('/signup', usuario.create);
router.get('/:id/update', usuario.updateGet);
router.post('/:id/update', usuario.update);
router.post('/:id/delete', usuario.delete);

module.exports = router;